pwd
ls -lrt
echo "gem version: `gem --version`"
echo "Install gems:"
gem list
echo "Gem env:"
gem env
echo "Bundle env:"
bundle env
echo "Install some gems manually"
gem install tzinfo-data
bundle lock --add-platform x86-mingw32 x86-mswin32 x64-mingw32 java
echo "Create rails-app"
rails new rails-app -m composer.rb
