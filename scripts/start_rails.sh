#!/bin/sh

PID_FILE=/app/rails-app/tmp/pids/server.pid

#cd rails-app
pwd
ls -l
yarn install --pure-lockfile
if [ "${RAILS_ENV}" = "production" ]; then
  rake assets:precompile
fi
if [ -f "${PID_FILE}" ]; then
  rm ${PID_FILE}
fi
#bundle exec unicorn -c config/unicorn.rb -E $RAILS_ENV -l 3000
bundle exec rails s -p 3000 -b '0.0.0.0' -e $RAILS_ENV
