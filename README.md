# Rails 5.2 starter app

- rails-bootstrap starter
- Bootstrap 3.3
- MySQL
- Unicorn
- Rspec
- Guard
- yarn
- SimpleForm


# Getting started

- `docker-compose -f docker/docker-compose.dev.yml build`
- `docker-compose -f docker/docker-compose.dev.yml up`

# Generate Rails project

`docker-compose docker/docker-compose.create-rails.yml  run --rm ruby`

> Database connection to Docker container is not supported, need to change the `config/database.yml` file manually.

- `bundle exec rake db:create:all`

> Rails 5.2 and mysql2 gem version are not compatible, see https://github.com/RailsApps/rails_apps_composer/issues/371

- Updated rails-composer to use a more recent version

> tzinfo-data gem seems mandatory

- Added tzinfo-data gem to rails-composer manually

# References

- https://github.com/mattbrictson/rails-template
- http://kasei-san.hatenablog.com/entry/2018/03/11/002752
- https://github.com/RailsApps/rails-composer
